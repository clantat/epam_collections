package com.clantat;

import java.util.*;

public class Service {
    public Map<String,Long> repeats (String s){

        Map<String, Long> result = new HashMap<>();
        List<String> list = split(s);
        for (String str : list) {
            if (!result.containsKey(str)) {
                result.put(str,(long)Collections.frequency(list, str));
            }
        }
        return result;
    }
    public Set<String> unique(String s)
    {
        return new HashSet<>(split(s));
    }
    public List<String> sortingDown(String s){
        List<String> list = new ArrayList<>(unique(s));
        list.sort((first, second) -> first.length() != second.length() ? (first.length() > second.length() ? -1 : 1) : second.compareTo(first));
        return list;
    }
    public List<String> sortingUp(String s){
        List<String> list = new ArrayList<>(unique(s));
        list.sort((first, second) -> first.length() != second.length() ? (first.length() > second.length() ? 1 : -1) : first.compareTo(second));
        return list;
    }
    public List<String> split(String s)
    {
        return Arrays.asList(s.split("[^a-zA-Zа-яА-Я0-9]+"));
    }

}
